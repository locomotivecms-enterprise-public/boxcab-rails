require 'rails/railtie'

module Boxcab
  class Railtie < ::Rails::Railtie

    initializer 'boxcab.action_view' do |app|
      ActiveSupport.on_load :action_view do
        require 'boxcab/action_view/helpers'
        include Boxcab::ActionView::Helpers
      end
    end

    initializer 'boxcab.action_contoller' do |app|
      ActiveSupport.on_load(:action_controller) do
        require 'boxcab/action_controller/controller'
        include Boxcab::ActionController::Controller
      end
    end

    initializer 'boxcab.middlewares' do |app|
      require 'boxcab/page_middleware'
      app.config.middleware.insert_after ActionDispatch::Static, Boxcab::PageMiddleware
    end

    initializer 'boxcab.assets' do |app|
      app.config.assets.paths << File.expand_path('../assets/javascripts', __FILE__)
      app.config.assets.precompile += %w(boxcab.js)
    end

    initializer 'boxcab.load_api' do |app|
      require 'boxcab/api'

      if (site_handle = ENV['BOXCAB_SITE_HANDLE']).blank? || (api_key = ENV['BOXCAB_API_KEY']).blank?
        abort '[Boxcab API] missing BOXCAB_SITE_HANDLE or/and BOXCAB_API_KEY env variables'
      end

      base_uri = ENV['BOXCAB_BASE_URI'] || 'https://editor.boxcab.io'

      $boxcab_api = Boxcab::API.new(base_uri, site_handle, api_key)
    end

    config.to_prepare do
      %w(high_voltage).each do |vendor|
        path = "boxcab/vendor/#{vendor}"
        Rails.application.config.cache_classes ? require(path) : load("#{path}.rb")
      end
    end

  end
end
