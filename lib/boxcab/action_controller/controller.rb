module Boxcab
  module ActionController
    module Controller

      extend ActiveSupport::Concern

      ALLOW_FROM = %(nossl-editor.boxcab.io editor.boxcab.io editor.boxcab.dev:5000)

      included do

        before_action :allow_boxcab_iframe

      end

      private

      def allow_boxcab_iframe
        if request.env['HTTP_USER_AGENT'] =~ /MSIE/
          response.headers['X-Frame-Options'] = "ALLOW-FROM #{ALLOW_FROM}"
        else
          response.headers.delete 'X-Frame-Options'
          response.headers['Content-Security-Policy'] = "frame-ancestors #{ALLOW_FROM}"
        end
      end

      module ClassMethods

        def boxcab_pages_here(only: nil)
          include BoxcabContent
          around_action :wrap_boxcab_page, only: only
        end

      end

      module BoxcabContent # Avoid potential name clashing (Content being too generic)

        extend ActiveSupport::Concern

        private

        def wrap_boxcab_page
          @boxcab_page = boxcab_service.find_or_build(request.original_url, params[:boxcab_content].try(:to_unsafe_h))

          yield.tap do
            case boxcab_service.save(@boxcab_page)
            when :success   then Rails.logger.info("[Boxcab] the page was saved")
            when :fail      then Rails.logger.error("[Boxcab] the page was not saved. Please email us at support@boxcab.io")
            when :unchanged then Rails.logger.info("[Boxcab] the page was not saved because of an unchanged schema")
            else
              Rails.logger.info "[Boxcab] the page was not saved because of the live preview mode or not in production"
            end
          end
        end

        def boxcab_service
          @boxcab_service ||= Boxcab::PageService.new($boxcab_api, Rails.env.production? || ENV['BOXCAB_BASE_URI'].present?)
        end

      end
    end
  end
end
