module Boxcab
  module ActionView
    module Helpers

      BOXCAB_CONTENT_TYPES = %(string text markdown image list)

      def boxcab_page(attributes)
        _boxcab_page.attributes = attributes.slice(:title, :position)
        ''
      end

      def boxcab_content(name, options = {}, &block)
        definition = { name: name.to_s, type: 'string' }.with_indifferent_access.merge(options)

        # do not store symbols
        definition[:type] = definition[:type].to_s

        value = (@boxcab_nested_content || _boxcab_page.content)[name]

        if BOXCAB_CONTENT_TYPES.include?(definition[:type])
          send(:"_boxcab_#{definition[:type]}", value, definition, &block).tap do
            _boxcab_append_definition(definition)
          end
        else
          raise "[Boxcab helper] Unknown type: #{definition[:type]}"
        end
      end

      def _boxcab_list(value, definition, &block)
        raise 'Too many nested lists' if @boxcab_nested_schema.present?
        raise 'You need to pass a template to your list type' unless block_given?

        # look for nested schema
        nested_schema = _boxcab_look_for_schema(&block)

        definition[:fields] = nested_schema

        # no need to look for definition for each item of the list
        @boxcab_no_schema = true

        # render list
        if value
          _boxcab_list_map(value.sort { |a, b| a['_position'] <=> b['_position'] }, &block)
        elsif definition[:default].is_a?(Integer)
          _boxcab_list_map(definition[:default].times, &block)
        else
          ''
        end.tap do
          @boxcab_no_schema = false
        end
      end

      def _boxcab_list_map(collection, &block)
        iterator = BoxcabViewListIterator.new(collection.size)

        collection.each_with_index.map do |content, index|
          @boxcab_nested_content = content.is_a?(Hash) ? content : {}
          capture(iterator.change(index), &block)
        end.tap do
          @boxcab_nested_content = nil
        end.join("\n").html_safe
      end

      def _boxcab_look_for_schema(&block)
        @boxcab_nested_schema = []
        capture(BoxcabViewListIterator.new, &block) # don't want to display it
        @boxcab_nested_schema.tap { @boxcab_nested_schema = nil }
      end

      def _boxcab_string(value, definition, &block)
        definition[:default] = capture(&block) if block_given?

        (value || definition[:default] || '').html_safe
      end

      alias _boxcab_text      _boxcab_string
      alias _boxcab_markdown  _boxcab_string
      alias _boxcab_image     _boxcab_string

      def _boxcab_page
        @boxcab_page
      end

      def _boxcab_append_definition(definition)
        return if @boxcab_no_schema

        schema = @boxcab_nested_schema || _boxcab_page.schema
        schema << definition
      end

      class BoxcabViewListIterator

        attr_reader :index, :size

        def initialize(size = 0, index = 0)
          @size, @index = size, index
        end

        def first?
          @index == 0
        end

        def last?
          @index == @size - 1
        end

        def every?(n)
          !first? && !last? && ((@index + 1) % n) == 0
        end

        def change(index)
          @index = index
          self
        end

      end

    end
  end
end
