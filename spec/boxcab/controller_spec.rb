require 'rails_helper'

RSpec.describe 'Controller powered by Boxcab', type: :controller do

  controller do
    boxcab_pages_here

    def index
      render plain: 'It worked!'
    end
  end

  let(:service) { controller.send(:boxcab_service) }

  describe 'include Boxcab controller methods' do

    it 'returns an instance of the Boxcab service' do
      expect(service).not_to eq nil
    end

  end

  describe 'wrap the actions' do

    it 'calls the service to find and create the page' do
      expect(service).to receive(:find_or_build).and_return('page')
      expect(service).to receive(:save).with('page')
      get :index
    end

  end
end
