Rails.application.routes.draw do

  root 'welcome#index'

  get '/pages/*page' => 'pages#show', as: :static_page

end
