require 'httparty'

module Boxcab
  class API

    include HTTParty

    def initialize(base_uri, site_handle, api_key)
      @site_handle = site_handle
      @options = {
        base_uri: base_uri,
        headers: {
          'x-boxcab-site-handle'  => site_handle,
          'x-boxcab-api-key'      => api_key
        }
      }
    end

    def find(id)
      _options = @options.slice(:base_uri)

      response = self.class.get("/api/v1/public/#{@site_handle}/pages/#{id}.json", _options)

      response.success? ? response.parsed_response : nil
    end

    def update_site(attributes)
      _attributes = attributes.slice(:name, :base_url)

      response = self.class.put("/api/v1/site.json", @options.merge(body: {
        site: _attributes
      }))

      response.success?
    end

    def create(attributes)
      response = self.class.post('/api/v1/pages.json', @options.merge(body: {
        page: attributes
      }))

      response.success? ? response.parsed_response : nil
    end

    def update(id, attributes)
      _attributes = attributes.slice('title', 'url', 'schema', 'position')
      _attributes['schema']   = _attributes['schema'].to_json

      response = self.class.put("/api/v1/pages/#{id}.json", @options.merge(body: {
        page: _attributes
      }))

      response.success?
    end

  end
end
