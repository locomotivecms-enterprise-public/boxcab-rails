require 'rails_helper'

describe 'Boxcab configuration' do

  before(:each) { Boxcab.reset }

  describe '#site_name' do

    before do
      Boxcab.configure do |config|
        config.site_name = 'My site name'
      end
    end

    it 'allows to set a title' do
      expect(Boxcab.configuration.site_name).to eq 'My site name'
    end

  end

  describe '#base_url' do

    let(:url) { 'big-flower-352' }

    before do
      Boxcab.configure { |config| config.base_url = url }
    end

    context 'base_url is the Heroku app name' do

      it 'builds the base_url from it' do
        expect(Boxcab.configuration.base_url).to eq 'https://big-flower-352.herokuapp.com'
      end

    end

    context 'base_url is the final site url' do

      let(:url) { 'https://www.acme.org' }

      it 'returns the site url' do
        expect(Boxcab.configuration.base_url).to eq 'https://www.acme.org'
      end

    end

  end

end
