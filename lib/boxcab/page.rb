module Boxcab

  class Page

    ATTRIBUTE_NAMES = %w(id title url path position schema content)

    def initialize(attributes = {})
      @attributes = {}.with_indifferent_access
      @read_only  = false

      unless attributes.blank?
        @previous_attributes = attributes.with_indifferent_access
        @attributes = @previous_attributes.with_indifferent_access
      end

      @attributes['content'] ||= {}

      # new schema in any case
      @attributes['schema'] = []
    end

    ATTRIBUTE_NAMES.each do |name|
      define_method(name) { @attributes[name] }
      define_method(:"#{name}=") { |value| @attributes[name] = value }
    end

    def attributes=(new_attributes)
      @attributes.merge!(new_attributes)
    end

    def self.build_default(url, path)
      new.tap do |page|
        page.title  = path.split('/').last.humanize
        page.url    = url
        page.path   = path
      end
    end

    def content=(content)
      @read_only = true
      @attributes['content'] = content
    end

    # only called before an existing page is going to be updated
    def changed?
      return true if @attributes.slice(:title, :position) != @previous_attributes.slice(:title, :position)

      @attributes[:schema] != @previous_attributes[:schema]
    end

    def read_only?
      @read_only
    end

    def persisted?
      !self.id.nil?
    end

    def to_api
      if persisted?
        @attributes.slice(:title, :position, :schema)
      else
        @attributes.slice(:title, :url, :path, :position, :schema)
      end
    end

  end

end
