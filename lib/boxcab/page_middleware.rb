module Boxcab

  class PageMiddleware

    BOXCAB_PATH       = /^\/_boxcab\/(.*)/o
    BOXCAB_PARAM_NAME = 'boxcab_content'

    def initialize(app)
      @app = app
    end

    def call(env)
      dup._call(env)
    end

    def _call(env)
      if env['PATH_INFO'] =~ BOXCAB_PATH
        change_request(env, $1)
        decode_content(env)
      end

      @app.call(env)
    end

    def change_request(env, path)
      path = "/#{path}"
      path = '/' if path == '/index'

      env['REQUEST_METHOD']     = 'GET'
      env['REQUEST_URI']        = path
      env['PATH_INFO']          = path
      env['ORIGINAL_FULLPATH']  = path
    end

    def decode_content(env)
      request = Rack::Request.new(env)
      content = request.params[BOXCAB_PARAM_NAME]

      return if content.blank?

      decoded   = Base64.decode64(content)
      _content  = ActiveSupport::JSON.decode(URI.unescape(decoded))

      request.update_param(BOXCAB_PARAM_NAME, _content)
    end

  end

end
