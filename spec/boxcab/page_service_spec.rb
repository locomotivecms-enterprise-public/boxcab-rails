require 'rails_helper'

describe Boxcab::PageService do

  let(:api) { instance_double('BoxcabAPI') }
  let(:service) { described_class.new(api) }

  describe '#find_or_build' do

    let(:url) { 'http://mysite.com/index' }
    let(:content) { nil }

    subject { service.find_or_build(url, content) }

    it 'calls the api to find a new page' do
      expect(api).to receive(:find).with('e3ee8a3d01d8d6147afd0cb3f30176b1').and_return(nil)
      expect(subject.title).to eq 'Index'
      expect(subject.url).to eq 'http://mysite.com/index'
      expect(subject.path).to eq '/index'
      expect(subject.content).to eq({})
      expect(subject.schema).to eq([])
      expect(subject.read_only?).to eq false
    end

    context 'with content (preview mode)' do

      let(:content) { { 'title' => 'Title' } }

      it 'bypasses the API' do
        expect(api).not_to receive(:find).with('e3ee8a3d01d8d6147afd0cb3f30176b1')
        expect(subject.title).to eq 'Index'
        expect(subject.url).to eq 'http://mysite.com/index'
        expect(subject.path).to eq '/index'
        expect(subject.content).to eq({ 'title' => 'Title' })
        expect(subject.read_only?).to eq true
      end

    end

  end

  describe '#save' do

    let(:page_id) { nil }
    let(:page)    { Boxcab::Page.new(id: page_id, title: 'Index', url: 'http://mysite.com/index', path: '/index') }

    subject { service.save(page) }

    it 'calls the api to create a new page' do
      expect(api).to receive(:create).with('title' => 'Index', 'url' => 'http://mysite.com/index', 'path' => '/index', 'schema' => []).and_return(true)
      is_expected.to eq :success
    end

    context 'not in production' do

      let(:service) { described_class.new(api, false) }

      it "doesn't call the API to create a new page" do
        expect(api).not_to receive(:create)
        is_expected.to eq :skip
      end

    end

    context 'the page already exists' do

      let(:page_id) { 42 }

      it 'calls the api to update the page' do
        expect(api).to receive(:update).with(42, { 'title' => 'Index', 'schema' => [] }).and_return(true)
        is_expected.to eq :success
      end

      context 'the schema has not changed' do

        it "doesn't call the API to update the page" do
          allow(page).to receive(:changed?).and_return(false)
          expect(api).not_to receive(:update)
          is_expected.to eq :unchanged
        end

      end

    end

  end

end
