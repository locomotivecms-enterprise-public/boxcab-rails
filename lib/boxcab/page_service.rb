module Boxcab
  class PageService

    def initialize(api, production = true)
      @api, @production = api, production
    end

    def find_or_build(url, content = nil)
      path = extract_path(url)
      page_id = Digest::MD5.hexdigest(path)

      page = (if content.blank? && attributes = @api.find(page_id)
        Boxcab::Page.new(attributes)
      else
        Boxcab::Page.build_default(url, path)
      end)

      page.content = content unless content.blank?

      page
    end

    def save(page)
      if !production? || page.read_only?
        :skip
      elsif page.persisted?
        return :unchanged unless page.changed?
        @api.update(page.id, page.to_api) ? :success : :fail
      else
        @api.create(page.to_api) ? :success : :fail
      end
    end

    def production?
      !!@production
    end

    private

    def extract_path(url)
      path = URI.parse(url).path
      path = '/index' if path.blank? || path == '/'
      path
    end

  end
end
