# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'boxcab/version'

Gem::Specification.new do |spec|
  spec.name          = "boxcab-rails"
  spec.version       = Boxcab::VERSION
  spec.authors       = ['did']
  spec.email         = ['didier@nocoffee.fr']

  spec.summary       = %q{Boxcab is an easy to use CMS for applications hosted on Heroku}
  spec.description   = %q{Boxcab Rails is the Ruby on Rails version of PerchCMS}
  spec.homepage      = "http://boxcab.io"
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'httparty', '~> 0.14.0'

  spec.add_development_dependency 'bundler', '~> 1.11'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'coffee-rails', '~> 4.2.1'
end
