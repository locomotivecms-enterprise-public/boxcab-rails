(->

  modifiedContent = false

  inIframe = ->
    try window.self != window.top catch e then true

  turbolinksEnabled = ->
    typeof(Turbolinks) != 'undefined'

  turbolinksEventNames = ->
    if typeof(Turbolinks.controller) != 'undefined'
      # Turbolinks 5
      { load: 'turbolinks:load', 'before': 'turbolinks:before-visit' }
    else
      # Turbolinks classic
      { load: 'page:load', 'before': 'page:before-change' }

  pathname = (url) ->
    parser = document.createElement('a')
    parser.href = url
    parser.pathname

  cleanPath = (path) -> if path == '/index' then '/' else path

  leaveMessage = -> "If you leave before saving, your changes will be lost"

  previewContent = (content) ->
    modifiedContent = true
    _previewContent content, (html) ->
      doc = document.documentElement.cloneNode()
      doc.innerHTML = html
      _html = doc.querySelector('body').innerHTML
      document.body.innerHTML = _html;
      document.dispatchEvent(new Event('boxcab:refresh'))

  _previewContent = (content, callback) ->
    path = '/_boxcab' + window.location.pathname

    xhr = new XMLHttpRequest()
    xhr.open 'PUT', path
    xhr.setRequestHeader 'Content-Type', 'application/x-www-form-urlencoded'
    xhr.setRequestHeader 'Accept', 'text/html'
    xhr.onload = ->
      if xhr.status == 200
        callback xhr.responseText
      else
        alert 'Application error. Check your site logs please.'

    xhr.send "#{encodeURIComponent('boxcab_content')}=#{encodeURIComponent(content)}"

  #
  # Common
  #
  setup = ->
    window.addEventListener 'message', (event) ->
      switch event.data.type
        when 'clean'    then modifiedContent = false
        when 'refresh'  then previewContent(event.data.content)
        when 'visit'    then modifiedContent = false
        else null

  #
  # Vanilla
  #
  setupVanilla = ->
    unloadTimeout = null

    document.addEventListener 'DOMContentLoaded', (event) ->
      path = window.location.pathname;
      if path == '/' then path = '/index'
      window.parent.postMessage({ type: 'pageLoaded', path: path }, '*')

    window.onbeforeunload = (event) ->
      if modifiedContent
        (event || window.event).returnValue = leaveMessage() # //Gecko + IE
        leaveMessage() # Gecko + Webkit, Safari, Chrome etc.
      else
        window.parent.postMessage({ type: 'changePage' }, '*')

    window.addEventListener 'message', (event) ->
      switch event.data.type
        when 'visit' then window.location.href = cleanPath(event.data.path)
        else null
    , false

  #
  # Turbolinks (Classic and 5)
  #
  setupForTurbolinks = ->
    currentPath     = null
    eventNames      = turbolinksEventNames()

    document.addEventListener eventNames.load, (event) ->
      if currentPath == null then currentPath = window.location.pathname
      if currentPath == '/' then currentPath = '/index'
      window.parent.postMessage({ type: 'pageLoaded', path: currentPath }, '*')

    document.addEventListener eventNames.before, (event) ->
      if modifiedContent && !confirm(leaveMessage())
        event.preventDefault()
        return

      modifiedContent = false
      currentPath = pathname(event.data.url)
      if currentPath == '/' then currentPath = '/index'
      window.parent.postMessage({ type: 'changePage' }, '*')

    window.addEventListener 'message', (event) ->
      switch event.data.type
        when 'clean'    then Turbolinks.clearCache()
        when 'visit'    then Turbolinks.visit(currentPath = cleanPath(event.data.path))
        else null
    , false

  if inIframe()
    setup()

    if turbolinksEnabled()
      console.log('[Boxcab][Front] Set up Turbolinks')
      setupForTurbolinks()
    else
      console.log('[Boxcab][Front] Set up Simple');
      setupVanilla()
)()
