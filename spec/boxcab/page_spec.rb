require 'rails_helper'

describe Boxcab::Page do

  describe '.new' do

    subject { described_class.new(id: 42, title: 'Hello world', schema: [1, 2, 3]) }

    it 'stores the values' do
      expect(subject.id).to eq 42
      expect(subject.title).to eq 'Hello world'
      expect(subject.schema).to eq([])
    end

  end

  describe '.build_default' do

    subject { described_class.build_default('http://acme.org/', 'index') }

    it 'sets default values' do
      expect(subject.id).to eq nil
      expect(subject.title).to eq 'Index'
      expect(subject.url).to eq 'http://acme.org/'
      expect(subject.path).to eq 'index'
      expect(subject.schema).to eq([])
      expect(subject.content).to eq({})
    end

  end

  describe '.read_only?' do

    let(:page) { described_class.new(title: 'Hello world') }

    subject { page.read_only? }

    it { is_expected.to eq false }

    context 'with content' do

      before { page.content = { 'title' => 'Title' } }

      it { is_expected.to eq true }

    end

  end

  describe '.changed?' do

    let(:schema) { [{ 'name' => 'title' }, { 'name' => 'features', 'fields' => [{ 'name' => 'name' }, { 'name' => 'picture' }] }] }
    let(:page) { described_class.new(title: 'Hello world', position: 1, schema: schema) }

    before { page.schema = schema.dup }

    subject { page.changed? }

    it 'returns false if no attribute has been modified' do
      is_expected.to eq false
    end

    context 'an attribute (either title or position) has changed' do

      before { page.title = 'Hello world!' }

      it { is_expected.to eq true }

    end

    context 'the schema has been modified' do

      context 'drop content definition' do

        before { page.schema = [{ 'name' => 'title' }] }

        it { is_expected.to eq true }

      end

      context 'existing content definition enhanced' do

        before { page.schema = schema.dup.tap { |s| s[0]['position'] = 2 } }

        it { is_expected.to eq true }

      end

      context 'adding a new field in a list content definition' do

        before { page.schema = schema.dup.tap { |s| s[1]['fields'] << { 'name': 'url' } } }

        it { is_expected.to eq true }

      end

    end

  end

end
