require 'boxcab/version'
require 'boxcab/configuration'
require 'boxcab/api'
require 'boxcab/page'
require 'boxcab/page_service'
require 'boxcab/railtie' if defined?(Rails)

module Boxcab

  class << self
    attr_writer :configuration
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.reset
    @configuration = Configuration.new
  end

  def self.configure
    yield(configuration)
  end

end
