require 'rails_helper'

describe 'Boxcab ActionView helpers', type: :helper do

  let(:boxcab_page) { Boxcab::Page.new(title: 'Hello world', path: '/index', position: 2) }

  before { assign(:boxcab_page, boxcab_page) }

  describe '#boxcab_page' do

    let(:attributes) { {} }

    subject { helper.boxcab_page(attributes) }

    it 'renders an empty string' do
      is_expected.to eq ''
      expect(boxcab_page.title).to eq('Hello world')
    end

    context 'passing a title' do

      let(:attributes) { { title: 'My home page', position: 1, index: '/foo' } }

      it 'sets the page title' do
        is_expected.to eq ''
        expect(boxcab_page.title).to eq('My home page')
        expect(boxcab_page.position).to eq(1)
        expect(boxcab_page.path).to eq('/index')
      end

    end

  end

  describe '#boxcab_content' do

    let(:name) { 'dummy' }
    let(:attributes) { {} }
    let(:block) { nil }

    subject { helper.boxcab_content(name, attributes, &block) }

    it 'returns an empty string by default' do
      is_expected.to eq ''
    end

    context 'string type' do

      let(:attributes) { { type: 'string', default: 'Hello world' } }

      it 'returns the default value defined in the options' do
        is_expected.to eq 'Hello world'
      end

      context 'the page already has the value' do

        let(:boxcab_page) { Boxcab::Page.new(content: { 'dummy' => 'value from the API' }) }

        it 'returns the existing value' do
          is_expected.to eq 'value from the API'
        end

      end

      context 'default is defined in the block' do

        let(:attributes) { { type: 'string' } }
        let(:block) { lambda { 'Hello world!' } }

        it 'uses the value returned by the block' do
          is_expected.to eq 'Hello world!'
        end

      end

    end

    context 'text type' do

      let(:attributes) { { type: 'text', default: 'Hello world' } }

      it 'returns the default value defined in the options' do
        is_expected.to eq 'Hello world'
      end

      context 'the page already has the value' do

        let(:boxcab_page) { Boxcab::Page.new(content: { 'dummy' => 'value from the API' }) }

        it 'returns the existing value' do
          is_expected.to eq 'value from the API'
        end

      end

      context 'default is defined in the block' do

        let(:attributes) { { type: 'string' } }
        let(:block) { lambda { 'Hello world!' } }

        it 'uses the value returned by the block' do
          is_expected.to eq 'Hello world!'
        end

      end

    end

    context 'markdown type' do

      let(:attributes) { { type: 'markdown', default: 'Hello world' } }

      it 'returns the default value defined in the options' do
        is_expected.to eq 'Hello world'
      end

      context 'the page already has the value' do

        let(:boxcab_page) { Boxcab::Page.new(content: { 'dummy' => 'value from the API' }) }

        it 'returns the existing value' do
          is_expected.to eq 'value from the API'
        end

      end

      context 'default is defined in the block' do

        let(:attributes) { { type: 'string' } }
        let(:block) { lambda { 'Hello world!' } }

        it 'uses the value returned by the block' do
          is_expected.to eq 'Hello world!'
        end

      end

    end

    context 'image type' do

      let(:attributes) { { type: 'image', default: 'http://assets/banner.png' } }

      it 'returns the default value defined in the options' do
        is_expected.to eq 'http://assets/banner.png'
      end

      context 'the page already has the value' do

        let(:boxcab_page) { Boxcab::Page.new(content: { 'dummy' => 'http://assets/new_banner.png' }) }

        it 'returns the existing value' do
          is_expected.to eq 'http://assets/new_banner.png'
        end

      end

    end

    context 'list type' do

      let(:attributes) { { type: 'list', default: 2 } }

      context 'no block passed' do

        it 'raises an exception' do
          expect { subject }.to raise_error('You need to pass a template to your list type')
        end

      end

      context 'block passed' do

        let(:block) { lambda { |iterator|
          helper.boxcab_content('title', default: 'Hello') +
          '[' + helper.boxcab_content('subtitle', default: 'world') + ']'
        } }

        it 'appends the nested schema' do
          is_expected.to eq "Hello[world]\nHello[world]"
          expect(helper._boxcab_page.schema.first[:fields]).to eq([
            { 'name' => 'title', 'type' => 'string', 'default' => 'Hello' },
            { 'name' => 'subtitle', 'type' => 'string', 'default' => 'world' }
          ])
        end

        context 'the page already has the value' do

          let(:boxcab_page) { Boxcab::Page.new(content: {
            'dummy' => [
              { 'title' => 'B', 'subtitle' => 'item #2', '_position' => 2 },
              { 'title' => 'C', 'subtitle' => 'item #3', '_position' => 3 },
              { 'title' => 'A', 'subtitle' => 'item #1', '_position' => 1 },
              { 'title' => 'D', 'subtitle' => 'item #4', '_position' => 4 }
            ]
          }) }

          it 'returns the existing value' do
            is_expected.to eq "A[item #1]\nB[item #2]\nC[item #3]\nD[item #4]"
          end

          describe 'iterator' do

            let(:block) { lambda { |iterator|
              helper.boxcab_content('title', default: 'Hello') +
              (iterator.first? ? ' is first' : '') +
              (iterator.last? ? ' is last' : '') +
              (iterator.every?(2) ? ' is 2nd' : '') +
              '[' + helper.boxcab_content('subtitle', default: 'world') + ']'
            } }

            it 'returns the elements with iterator information' do
              is_expected.to eq "A is first[item #1]\nB is 2nd[item #2]\nC[item #3]\nD is last[item #4]"
            end

          end

        end

      end

    end

  end

end
