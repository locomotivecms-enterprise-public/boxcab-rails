# FIXME (Did) Not used for now
module Boxcab
  class Configuration

    attr_accessor :site_name
    attr_accessor :base_url

    def base_url=(value)
      @base_url = (if value =~ /^[a-zA-Z0-9-]+$/
        "https://#{value}.herokuapp.com"
      else
        value
      end)
    end

    def to_site_attributes
      {
        name:     self.site_name,
        base_url: self.base_url
      }.delete_if { |k, v| v.blank? }
    end

  end
end
