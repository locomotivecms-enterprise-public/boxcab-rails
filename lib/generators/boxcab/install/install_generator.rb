class Boxcab::InstallGenerator < Rails::Generators::Base

  # source_root File.expand_path('../templates', __FILE__)

  # FIXME (Did): disabled for now
  # def copy_templates
  #   template 'initializer.rb', 'config/initializers/boxcab.rb'
  # end

  def ask_for_site_attributes
    print_title "Boxcab site configuration"
  end

  def ask_for_name
    say <<-EOF
You can change the name of your site which is displayed in the Boxcab editor.
Leave it blank if you don't want to change it.

EOF
    @site_name = ask("What's its name?")
  end

  def ask_for_base_url
    say <<-EOF

For a smoother experience, Boxcab requires the base url (http or https + ://<your domain>) of your site in production
OR if you don't have one yet your Heroku app name.

EOF
    @site_base_url = ask("What's your base url OR your Heroku app name?")
  end

  def update_site
    attributes = {}
    attributes[:name]     = @site_name unless @site_name.strip.blank?
    attributes[:base_url] = @site_base_url unless @site_base_url.strip.blank?

    if !attributes.blank? && $boxcab_api.update_site(attributes)
      say "\n"
      say "Your Boxcab site has been updated!", [:green, :on_black, :bold]
    end
  end

  def print_asset_instruction
    say "\n\n"

    print_title "Boxcab JS lib installation"

    say <<-EOF
In order to enable the live editing functionality, you have to add our javascript lib.
Add the following statement in your layout or your pages you want to edit before the </head> tag.

ERB:
  <%= javascript_include_tag 'boxcab' %>

HAML/SLIM
  = javascript_include_tag 'boxcab'


    EOF
  end

  def print_next_instructions
    print_title "Boxcab make pages editable"

    say <<-EOF

Please visit the following links for the next instructions:

  - http://www.boxcab.io/pages/documentation/installation#setup-it
  - http://www.boxcab.io/pages/documentation/usage


EOF
  end

  private

  def print_title(title)
    @step = @step ? @step + 1 : 1
    say "#{@step}. #{title}", [:white, :on_blue, :bold]
    say ''
  end

end
