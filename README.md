# Boxcab for Ruby on Rails applications

Boxcab is an easy to use CMS for applications hosted on **Heroku**. It allows non-tech people to edit the content of static pages through a nice UI.
See it as [PerchCMS](http://grabaperch.com) for Heroku applications.

There is nothing to host, no migration to run. It works with Rails 3, Rails 4 and Rails 5.

## What are static pages?

Usually we're talking about the "about us", "our team", contact us", etc. pages in your Rails application. In some cases, we can extend this definition to the index page.

There are many ways to handle them. You can either roll your own system like described in this article [http://blog.teamtreehouse.com/static-pages-ruby-rails](http://blog.teamtreehouse.com/static-pages-ruby-rails) or use a gem like [HighVoltage](https://github.com/thoughtbot/high_voltage).

The awesome thing is that Boxcab is pretty agnostic about your system.

## Requirements ##

Your application has to run on Heroku. Besides, you have to add the **Boxcab** add-on.

```
heroku addons:create boxcab
```

## Installation

Follow the [instructions here]([Heroku documentation](https://devcenter.heroku.com/articles/boxcab#ruby-on-rails-installation).

## Usage

Please visit our documentation site: [http://www.boxcab.io/pages/documentation/usage](http://www.boxcab.io/pages/documentation/usage).

## FAQ

[http://www.boxcab.io/pages/faq](http://www.boxcab.io/pages/faq).

## Development

After checking out the repo, run `rake spec` to run the tests.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/locomotivecms-enterprise-public/boxcab-rails.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

