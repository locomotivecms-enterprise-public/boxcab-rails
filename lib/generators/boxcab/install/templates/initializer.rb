# Boxcab configuration
#
# Once you've made a modification to this file, run:
# $ rails boxcab:apply_configuration
#
# This will set the settings in production
#

Boxcab.configure do |config|

  # In the Boxcab editor, name displayed at the top in the left sidebar.
  # By default, the Heroku app name will be chosen.
  # config.site_name = 'demo'

  # Base url of your site when running on Heroku. You can either pass
  # your Heroku app name OR the url if you attached a domain to your Heroku app.
  #
  # config.base_url = "my-awesome-site"
  # or
  # config.base_url = "https://myawesomesite.org"
  #

end

