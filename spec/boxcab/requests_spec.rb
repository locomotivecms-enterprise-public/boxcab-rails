require 'rails_helper'

describe 'Pages powered by boxcab', type: :request, vcr: { cassette_name: 'remote_api', record: :new_episodes } do

  describe 'GET /index' do

    subject do
      get('/', { params: {}, headers: {} })
    end

    it 'returns a HTML page with default content' do
      is_expected.to eq 200
      expect(response.body).to include('My home page')
      expect(response.body).to include('<p>Simple text</p>')
      expect(response.body).not_to include('<div class="background">')
      expect(response.body).to include('<img class="media-object" src="http://placehold.it/200x200" />')
    end

  end

  describe 'GET /pages/about' do

    subject do
      get('/pages/about', { params: {}, headers: {} })
    end

    it 'returns a HTML page with content from boxcab' do
      is_expected.to eq 200
      expect(response.body).to include('About us')
      expect(response.body).to include('<p>Content from the API!</p>')
    end

  end

  describe 'PUT /_boxcab/index' do

    let(:params) { { boxcab_content: encode_content({ 'introduction' => 'Simple text got modified!' }) } }

    subject do
      put('/_boxcab/index', { params: params, headers: {} })
    end

    it 'returns a HTML page with content from the params' do
      is_expected.to eq 200
      expect(response.body).to include('My home page')
      expect(response.body).to include('Simple text got modified!')
    end

  end

  def encode_content(content)
    Base64.encode64(ActiveSupport::JSON.encode(content))
  end

end
